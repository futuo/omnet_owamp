//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
// 
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
// 

#pragma once

#include <cstring>
#include <cstdlib>

#include "INETDefs.h"
#include "UDPSocket.h"
#include "IPvXAddressResolver.h"
#include "UDPControlInfo_m.h"

#include "owrng.h"
#include "ow_defs.h"
#include "scheduler.h"
#include "owpacket_m.h"
#include "owprobe_m.h"
#include "requestSession_m.h"
#include "startSession_m.h"
#include "stopSession_m.h"


#define max(i,j) ((i)>(j) ? (i):(j))

class OwClient : public cSimpleModule
{
private:

    enum ClientState{

        SESSION_REQUEST_NOT_SENT,
        SESSION_REQUEST_SENT,
        SESSION_REQUEST_ACCEPTED,

        SESSION_START_NOT_SENT,
        SESSION_START_SENT,
        SESSION_IS_ACTIVE,
        SESSION_IS_INACTIVE,

        SESSION_TERMINATED

    };

    ClientState curState;
    std::list<int> skippedList;
    
    //Connection Stuff
    UDPSocket socket;
    int localPort, destPort;
    IPvXAddress destAddr;
    int testServerPort;
    int sessionAccepted;
        
    //session info
    simtime_t sessionStart;
    long sessionID;
    
    //probes info
    int nextSequenceNumber;
    int probeSize;
    double timeout;
    double averageProbeInterval;
    double duration;
    int nPackets;
    std::string scheduleDescription;
    Scheduler scheduler;

    //self message vars
    simtime_t nextProbe;
    simtime_t nextClientReminder;
    cMessage *clientReminderMsg;

    //multiple followed session vars
    int sessionCount;
    int nSessions;
    simtime_t sessionInterval;

    public:

        void initializeScheduler( std::string );

        virtual OwProbe *createProbe();
        virtual void handleMessage(cMessage *msg);

        void takeNextStep();
        void verifySkippedProbes();

        virtual int numInitStages() const {return 4;}
        virtual void initialize(int stage);

        RequestSession *createRequestSession();
        OwStartSession *createStartSession();
        OwStopSession *createStopSession();

        virtual void finish();

        OwClient();
        ~OwClient();

};

