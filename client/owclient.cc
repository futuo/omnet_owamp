//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
// 
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
// 

#include "owclient.h"

using std::cout;
using namespace std;

Define_Module(OwClient);

OwClient::OwClient() {
    OwClient::sessionCount = 0;
    OwClient::sessionAccepted = 0;
    OwClient::nextSequenceNumber = 1;
    OwClient::clientReminderMsg = NULL;
    OwClient::scheduler = Scheduler();
}

OwClient::~OwClient() {
    cancelAndDelete(clientReminderMsg);
}

void OwClient::initialize(int stage)
{
    // because of IPvXAddressResolver, we need to wait until interfaces are registered,
    // address auto-assignment takes place etc.
    if (stage != 3)
        return;

    cout << "initializing owamp application" << endl;

    //read multiple followed session parameters
    nSessions       = par("nSessions");
    sessionInterval = par("sessionInterval");

    //set up connection with server
    localPort = par("localPort");
    destPort  = par("destPort");
    destAddr  = IPvXAddressResolver().resolve( par("destAddr") );
    
    socket.setOutputGate(gate("udpOut"));
    socket.bind(localPort);
    socket.setTimeToLive(32);

    //read session configuration parameters
    probeSize            = par("probeSize").longValue();
    timeout              = par("timeout").doubleValue();
    nPackets             = par("nPackets");
    scheduleDescription  = par("scheduleDescription").stringValue();
    duration             = par("duration");

    //initialize the scheduler
    initializeScheduler( scheduleDescription );

    //read session start time
    sessionStart = par("startTime").doubleValue();
    
    //set current state
    curState = SESSION_REQUEST_NOT_SENT;
    
    //schedule the first session event to max( sessionStart-5, 0 )
    nextProbe = sessionStart;
    nextClientReminder = max( sessionStart-5.0, 0 );
    clientReminderMsg = new cMessage("client reminder");
    scheduleAt(nextClientReminder, clientReminderMsg);

}

//parses the string parameter for the scheduling description
void OwClient::initializeScheduler( string descript ){

    string::iterator i = descript.begin();

    scheduler = Scheduler();
    schedule_t type;
    double interval;
    unsigned int seed;

    bool isTrain  = false;
    int trainSize = 0;

    while( i != descript.end() ){
        //read first letter and bind distribution
        if(*i == 'e')
            type = is_exponential;
        else if(*i == 'p')
            type = is_periodic;
        else if(*i == 't'){
            isTrain = true;
        }

        //skip the letter
        i++;

        //if this is supposed to be a train of packets
        if( isTrain ){

            while(*i != 'e' && *i != 'p'){
                trainSize *= 10;
                trainSize += *i - '0';
                i++;
            }

            if(*i == 'e')
                type = is_exponential;
            else if(*i == 'p')
                type = is_periodic;
            i++;

            //insert the train of packets into the scheduler
            for( int i = 0; i < trainSize -1; i++ )
                scheduler.insertPeriodicSchedule(0);

        }

        //read characters up until a space appears or the string ends
        string parsedPeriod = "";
        while( i != descript.end() && *i != ' ' ){
            parsedPeriod += *i;
            i++;
        }
        interval = atof(parsedPeriod.c_str());

        switch(type){
            case is_exponential:
                //TODO what do we do about seeds?
                seed = rand();
                seed = 1;
                scheduler.insertExponentialSchedule(seed, interval); break;
            case is_periodic:
                scheduler.insertPeriodicSchedule(interval); break;
            default: break;
        }

        if(i != descript.end())
            i++;
    }

}


//handles incoming messages
void OwClient::handleMessage(cMessage *msg)
{
    //if is a "timing" message
    if (msg->isSelfMessage()) {
        takeNextStep();
    }else{

        OwPacket *owpkt = (OwPacket *)msg;
        if( owpkt->getType() == IS_REQUEST_SESSION_RESPONSE ){
            
            cout << "received request session response" << endl;

            RequestSessionResponse *res = (RequestSessionResponse*)owpkt;
            if(res->getAccept() == 0){
                OwClient::testServerPort = res->getServerPort();
                OwClient::sessionID      = res->getSessionID();
                curState = SESSION_REQUEST_ACCEPTED;
            }
            
            takeNextStep();

        }else if( owpkt->getType() == IS_START_SESSION_ACK ){
            
            cout << "received start session ack" << endl;

            OwStartSessionAck *start = (OwStartSessionAck*)owpkt;
            if( start->getAccept() == 0 )
                curState = SESSION_IS_ACTIVE;
            
            takeNextStep();

        }else if( owpkt->getType() == IS_STOP_SESSION ){

            cout << "received a stop session message" << endl;

            curState = SESSION_TERMINATED;
            takeNextStep();

        }else{
            cout << "unhandled control message reached client" << endl;
        }

        delete msg;
    }
}

void OwClient::finish()
{
    /*
    cout << endl << "---one way delay--- " << endl << "average: " << packetsReceived.computeOWDelay() << endl;

    double *netcap = packetsReceived.computeNetworkCapacity();
    cout << "---network capacty---" << endl;
    cout << "link used: " << netcap[0] << endl;
    cout << "link usage: " << netcap[1] << endl;
    cout << "available link capacity: " << netcap[2] << endl;
    */
}






/*************************************************
            the engine behind the client
 *************************************************/
void OwClient::takeNextStep(){
    switch( curState ){
        
        case SESSION_REQUEST_NOT_SENT:

            cout << "about to send session request" << endl;

            if( duration > 0.0 ){
        
                nPackets = 0;
                double length = 0.0;
                
                Scheduler auxSched = scheduler.clone();
                while( length < duration ){
                    length += auxSched.getNextValue();
                    nPackets++;
                }
            }

            //send request session command
            socket.sendTo( createRequestSession(), destAddr, destPort );

            cout << "sent session request" << endl;
            curState = SESSION_REQUEST_SENT;
        break;


        case SESSION_REQUEST_SENT:
            //TODO USELESS!!!! LOL =-)
            cout << "awaiting request ack" << endl;
        break;


        case SESSION_REQUEST_ACCEPTED:
            
            //schedule start session command
            nextClientReminder = max( sessionStart - 3.0, simTime() );
            scheduleAt(nextClientReminder, clientReminderMsg);
            curState = SESSION_START_NOT_SENT;

        break;


        case SESSION_START_NOT_SENT:
            //send request session command
            socket.sendTo( createStartSession(), destAddr, destPort );
            curState = SESSION_START_SENT;
            //verify if there are skipped packets
        break;


        case SESSION_START_SENT:
            //TODO USELESS!!!! LOL =-)
            cout << "awaiting session start ack" << endl;
        break;

        case SESSION_IS_ACTIVE:

            //verify whether there are skipped packets
            verifySkippedProbes();

            if ( nextSequenceNumber >= nPackets ){
                //schedule stop session command
                curState = SESSION_IS_INACTIVE;
                nextClientReminder += timeout;
                scheduleAt(nextClientReminder, clientReminderMsg);
                break;
            }

            if ( nextProbe > simTime() ){
                nextClientReminder = nextProbe;
                scheduleAt(nextClientReminder, clientReminderMsg);
                break;
            }

            //send probe

            /*
            cout << "sent probe " << nextSequenceNumber << " at: "
                    << simTime() << " to port " << testServerPort << endl;
            */

            socket.sendTo(createProbe(), destAddr, testServerPort);

            //schedule next probe
            nextClientReminder = nextProbe += scheduler.getNextValue();
            scheduleAt(nextClientReminder, clientReminderMsg);

        break;

        case SESSION_IS_INACTIVE:
            socket.sendTo( createStopSession(), destAddr, destPort );
        break;

        case SESSION_TERMINATED:
            
            //increment the number of sessions performed so far
            sessionCount++;

            //check if there are more session to perform
            if( sessionCount < nSessions ){
                curState = SESSION_REQUEST_NOT_SENT;
                nextSequenceNumber = 1;
                scheduler.reset();
                sessionStart = simTime() + sessionInterval;
                nextProbe = sessionStart;
                nextClientReminder = max( sessionStart-5.0, simTime() );
                scheduleAt(nextClientReminder, clientReminderMsg);
            }

        break;

    }

}

void OwClient::verifySkippedProbes()
{
    //while the value of nextProbe is less than simTime()
    //add next Seqno to the skipped list and 
    //increment next sequence number

    while( nextProbe < simTime() ){
        
        cout << "skipped probe " << nextSequenceNumber << "supposed to leave at" << nextProbe << " : " << simTime() << endl;
        skippedList.insert( skippedList.end(), nextSequenceNumber );
        
        nextProbe += scheduler.getNextValue();
        cout << "trying: " << nextProbe << endl;
        nextSequenceNumber++;

    }

}




/*************************************************
            methods for packet creation
 *************************************************/


//creates and returns a new packet with the next sequence number
OwProbe *OwClient::createProbe()
{
    char msgName[32];
    sprintf(msgName, "owtest::probe_%d", nextSequenceNumber);

    OwProbe *probe = new OwProbe(msgName);
    probe->setType(IS_PROBE);

    probe->setByteLength(probeSize);
    probe->setProbeSize(probeSize);
    probe->setSessionID(sessionID);
    probe->setSequenceNumber( nextSequenceNumber );
    probe->setSendTime( simTime() );
    nextSequenceNumber++;

    return probe;
}

RequestSession *OwClient::createRequestSession()
{
    //create the request session command
    RequestSession *requestSession = new RequestSession("session request");
    requestSession->setType(IS_REQUEST_SESSION);
    //load values into packet
    requestSession->setNpackets(nPackets);
    requestSession->setClientPort(localPort);
    requestSession->setProbeSize(probeSize);
    requestSession->setTimeout(timeout);
    requestSession->setSessionStart(sessionStart);

    std::list<Schedule> schedules = scheduler.getSchedules();
    std::list<Schedule>::iterator it = schedules.begin();
    int i = 0;
    SlotDescriptor sd;
    
    requestSession->setNScheduleSlots(schedules.size());
    requestSession->setSlotDescriptorsArraySize( schedules.size() );
    for( ; it != schedules.end(); it++  ){
        sd.setDistribution( it->getType() );
        sd.setSeed( it->getSeed() );
        sd.setMean( it->getPeriod() );
        requestSession->setSlotDescriptors( i, sd );
        i++;
    }

    return requestSession;
}

OwStartSession *OwClient::createStartSession()
{
    //create the start session command
    OwStartSession *startSession = new OwStartSession("start session");
    startSession->setType(IS_START_SESSION);
    //load values into packet
    startSession->setSessionID(sessionID);

    return startSession;
}

OwStopSession *OwClient::createStopSession()
{
    int prev = -1, nSkipRanges = 0;
    std::list<int>::iterator it;

    for( it = skippedList.begin(); it != skippedList.end(); it++){
        if( prev == -1 || *it-prev > 1 )
            nSkipRanges++;
        prev = *it;
    }

    OwStopSession *stopSession = new OwStopSession("stop session");
    stopSession->setType(IS_STOP_SESSION);

    stopSession->setAccept( 0 );
    stopSession->setNSessions( 1 );
    stopSession->setSessionDescriptorsArraySize( 1 );

    SessionDescriptor sd;
    sd.setSessionID(sessionID);
    sd.setNextSeqno(nextSequenceNumber);
    sd.setNSkipRanges(nSkipRanges);
    sd.setSkipRangesArraySize(nSkipRanges);

    SkipRange sr;
    int set = 0, count = 0;
    prev = -1;

    for( it = skippedList.begin(); it != skippedList.end(); it++){
        if( prev == -1 || *it-prev > 1 ){
            //if skip range has interuption then add skip range to list
            if(set){
                sd.setSkipRanges(count, sr);
                count++;
                set = 0;
            }

            //start new skip range
            sr.setFirstSeqNo(*it);
            sr.setLastSeqNo(*it);
            set = 1;

        }else{
            //update current skip range
            sr.setLastSeqNo(*it);
        }
        //keep last skipped sequence number
        prev = *it;
    }
    //add last skip range
    if(set){
        sd.setSkipRanges(count, sr);
        count++;
    }

    stopSession->setSessionDescriptors( 0, sd );

    return stopSession;
}


