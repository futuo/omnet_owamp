//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
// 
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
// 

#pragma once

#include <vector>
#include <map>

#include "INETDefs.h"
#include "UDPSocket.h"
#include "UDPControlInfo_m.h"
#include "IPvXAddressResolver.h"

#include "owpacket_m.h"
#include "owrng.h"
#include "owsession.h"
#include "owprobe_m.h"
#include "requestSession_m.h"
#include "startSession_m.h"
#include "stopSession_m.h"

class INET_API OwServer : public cSimpleModule{

  private:

    std::map <int, OwSession*> sessions;

    /*These two shall be moved in a near future*/
    UDPSocket socket;
    int localPort;
    
    int nextSessionID;

  public:
    virtual void processPacket(cPacket *pk);
    virtual int numInitStages() const {return 4;}
    virtual void initialize(int stage);
    virtual void handleMessage(cMessage *msg);
    virtual void finish();

    void proccessStopSession(OwStopSession *stopSession);

    OwStartSessionAck *createStartSessionAck();
    OwStopSession *createStopSessionAck(OwSession *);

    OwServer();
    ~OwServer();
};

