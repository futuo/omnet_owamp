//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
// 
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
// 

#include "owserver.h"
#include <utility>
#include <cstdio>

using namespace std;
using std::cout;

Define_Module(OwServer);

OwServer::OwServer() {
    nextSessionID = 1;
}

OwServer::~OwServer() {
}

void OwServer::initialize(int stage)
{

    if(stage != 3) return;

    localPort = par("localPort");

    socket.setOutputGate(gate("udpOut"));
    socket.bind(localPort);
    socket.setTimeToLive(32);

}

//handles incoming messages
void OwServer::handleMessage(cMessage *msg)
{
    if( !msg->isSelfMessage() ){
        processPacket( PK(msg) );
    }
}

//proccesses received packets
void OwServer::processPacket(cPacket *pk)
{
    if (pk->getKind() == UDP_I_ERROR) {
        EV << "UDP error received\n";
        delete pk;
        return;
    }
    
    OwPacket *owpkt = (OwPacket*)pk;
    if( owpkt->getType() == IS_PROBE ){
        
        OwProbe *owprobe = (OwProbe*)pk;
        owprobe->setTtl(((UDPDataIndication *)(pk->getControlInfo()))->getTtl());
        OwSession *session = sessions.find( owprobe->getSessionID() )->second;
        session->announceProbeReceival(owprobe, simTime());

    }else if( owpkt->getType() == IS_REQUEST_SESSION ){

        RequestSession *request = (RequestSession *)pk;
        
        //retrieve client's address
        request->setClientAddress(((UDPDataIndication *)(pk->getControlInfo()))->getSrcAddr());

        //initialize the session
        request->setServerPort(localPort + nextSessionID);
        OwSession *newSession = new OwSession( nextSessionID, gate("udpOut"), request);
        sessions.insert( pair<int, OwSession *>( nextSessionID, newSession ) );
        newSession->setCurState(SESSION_REQUEST_RECEIVED);

        //send session request response
        RequestSessionResponse *response = new RequestSessionResponse("session request ack");
        response->setType(IS_REQUEST_SESSION_RESPONSE);
        response->setAccept(0);
        response->setServerPort(newSession->getServerPort());
        response->setSessionID(nextSessionID);
        
        socket.sendTo(response, newSession->getClientAddress(), newSession->getClientPort());

        cout << "server sent request session ack" << endl;

        nextSessionID++;

    }else if( owpkt->getType() == IS_START_SESSION ){
        
        //find and initialize correspondent session
        OwStartSession *start = (OwStartSession *)owpkt;
        OwSession *s = sessions.find( start->getSessionID() )->second;
        s->initSession();

        //send start session ack
        socket.sendTo(createStartSessionAck(), s->getClientAddress(), s->getClientPort());

    }else if( owpkt->getType() == IS_STOP_SESSION ){

        //process stop session received
        OwStopSession *stop = (OwStopSession *)owpkt;

        //iterate over all sessions
        SessionDescriptor sd;
        SkipRange sr;
        for(unsigned int i = 0; i < stop->getSessionDescriptorsArraySize(); i++){

            sd = stop->getSessionDescriptors(i);

            //find corresponding session
            OwSession *session = sessions.find( sd.getSessionID() )->second;

            //discard unsent packets
            for(int j=0; j<sd.getNSkipRanges(); j++){
                sr = sd.getSkipRanges(j);
                for( int k = sr.getFirstSeqNo(); k <= sr.getLastSeqNo(); k++ ){
                    session->getPacketHistory().find(k)->second->setDiscarded( 1 );
                }
            }
            
            //discard packets received between now-timeout and now
            OwProbeInfo * pi;

            for( int i = 1 ; i < session->getNpackets(); i++ ){
                pi = session->getPacketHistory().find(i)->second;

                if( pi->getReceivedtime() > simTime() - session->getTimeout() ){
                    pi->setDiscarded(1);
                }
            }

            //create the stop session ack and send it
            socket.sendTo(createStopSessionAck(session), session->getClientAddress(), session->getClientPort());
            
            //compute session metrics
            cout << endl << endl << "results for session with ID " << sd.getSessionID() << endl;
            session->generateResults();

            //clear session
            delete session;

        }

    }else{
        cout << "unknown message reached OWAMP control server" << endl;
    }

    delete pk;
    
}


void OwServer::finish()
{
    /*
    std::map <int, OwSession*>::iterator it;
    OwSession *session;

    for(it = sessions.begin(); it != sessions.end(); it++){
        session = it->second;
        cout << endl << endl << "results for session with ID " << it->first << endl;
        session->generateResults();
    }
    */

    /*
    cout << endl << "---one way delay--- " << endl << "average: " << packetsReceived.computeOWDelay() << endl;

    double *netcap = packetsReceived.computeNetworkCapacity();
    cout << "---network capacty---" << endl;
    cout << "link used: " << netcap[0] << endl;
    cout << "link usage: " << netcap[1] << endl;
    cout << "available link capacity: " << netcap[2] << endl;
    */
}

void proccessStopSession(OwStopSession *stopSession){
    //TODO
    /*
    std::map <int, OwSession*>::iterator it;
    OwSession *session;

    for(it = sessions.begin(); it != sessions.end(); it++){
        session = it->second;
        cout << endl << endl << "results for session with ID " << it->first << endl;
        session->generateResults();
    }
    */
}

OwStartSessionAck *OwServer::createStartSessionAck(){
    OwStartSessionAck *startAck = new OwStartSessionAck( "start session ack" );
    startAck->setType(IS_START_SESSION_ACK);
    startAck->setAccept(0);
    return startAck;
}

OwStopSession *OwServer::createStopSessionAck(OwSession *session){
    OwStopSession *stop = new OwStopSession("stop session ack");

    stop->setType(IS_STOP_SESSION);

    stop->setAccept(0);
    stop->setNSessions(1);
    stop->setSessionDescriptorsArraySize(1);

    SessionDescriptor sd;
    sd.setSessionID(session->getSessionID());

    int prev = -1, nSkipRanges = 0;
    for( int i = 1; i < session->getNpackets(); i++ ){
        if( session->getPacketHistory().find(i)->second->isDiscarded() ){
            if( prev == -1 || i-prev > 1 )
                nSkipRanges++;
            prev = i;
        }
    }

    sd.setNSkipRanges(nSkipRanges);
    sd.setSkipRangesArraySize(nSkipRanges);

    SkipRange sr;
    int set = 0, count = 0;
    prev = -1;

    for( int i = 1; i < session->getNpackets(); i++ ){
        if( session->getPacketHistory().find(i)->second->isDiscarded() ){
            if( prev == -1 || i-prev > 1 ){
                //if skip range has interuption then add skip range to list
                if(set){
                    sd.setSkipRanges(count, sr);
                    count++;
                    set = 0;
                }

                //start new skip range
                sr.setFirstSeqNo(i);
                sr.setLastSeqNo(i);
                set = 1;

            }else{
                //update current skip range
                sr.setLastSeqNo(i);
            }
            //keep last skipped sequence number
            prev = i;
        }
    }
    //add last skip range
    if(set){
        sd.setSkipRanges(count, sr);
        count++;
    }

    stop->setSessionDescriptors( 0, sd );

    return stop;

}
