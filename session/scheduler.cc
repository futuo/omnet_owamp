//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
// 
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
// 

#include <iostream>
#include "scheduler.h"

/*schedule constructors*/
Schedule::Schedule(schedule_t type, double period){
	Schedule::type = type;
	Schedule::period = period;
}
Schedule::Schedule(schedule_t type, double period, int seed){
	Schedule::type = type;
	Schedule::period = period;
	Schedule::seed = seed;
}
Schedule::Schedule(Schedule *another){
	Schedule::type = another->getType();
	Schedule::period = another->getPeriod();
	Schedule::seed = another->getSeed();	
}

Schedule Schedule::clone(){
	return Schedule(this);
}

/*setters*/
void Schedule::setType(schedule_t type){
	Schedule::type = type;
}
void Schedule::setSeed(unsigned int seed){
	Schedule::seed = seed;
}
void Schedule::setPeriod(double period){
	Schedule::period = period;
}

/*getters*/
schedule_t Schedule::getType(){
	return Schedule::type;
}
unsigned int Schedule::getSeed(){
	return Schedule::seed;
}
double Schedule::getPeriod(){
	return Schedule::period;
}



/*****Scheduler*****/

Scheduler::Scheduler(){
	pointerPosition = -1;
}

Scheduler::Scheduler(Scheduler *another){
	
	pointerPosition = another->getPointerPosition();
	
	std::list<Schedule> s = another->getSchedules();
	std::list<Schedule>::iterator it;

	for( it = s.begin(); it != s.end(); it++ ){
		schedules.insert( schedules.end(), it->clone() );	
	}

	pointerPosition = another->getPointerPosition();
	
	curSchedule = schedules.begin();
	for( int i = 0; i<pointerPosition; i++ )
		curSchedule++;

}

/*schedule inserters*/
void Scheduler::insertPeriodicSchedule( double period ){
	Schedule sc = Schedule(is_periodic, period);
	schedules.insert( schedules.end(), sc );
}

void Scheduler::insertUniformSchedule( int seed, double average ){
	Schedule sc = Schedule(is_uniform, average, seed);
	schedules.insert( schedules.end(), sc );
}

void Scheduler::insertExponentialSchedule( int seed, double average ){
	Schedule sc = Schedule(is_exponential, average, seed);
	schedules.insert( schedules.end(), sc );
}

int Scheduler::getNumScheduleSlots(){
	return schedules.size();
}

int Scheduler::getPointerPosition(){
	return pointerPosition;
}

std::list<Schedule> Scheduler::getSchedules(){
	return schedules;
}

Scheduler Scheduler::clone(){
	return Scheduler(this);
}

void Scheduler::reset(){
    Scheduler::curSchedule = Scheduler::schedules.begin();
    pointerPosition = 0;
}

/*next value generator*/
double Scheduler::getNextValue(){
	

	if( Scheduler::curSchedule == Scheduler::schedules.end() || pointerPosition < 0 ){
		Scheduler::curSchedule = Scheduler::schedules.begin();
		pointerPosition = 0;
	}

	double returnValue;

	switch( Scheduler::curSchedule->getType() ){
		case is_periodic:
			returnValue = Scheduler::curSchedule->getPeriod();
		break;

		case is_uniform:
			Scheduler::rng.seedRand( Scheduler::curSchedule->getSeed() );
			returnValue = Scheduler::rng.getNormalizedRand();
			Scheduler::curSchedule->setSeed( Scheduler::rng.getSeed() );
		break;

		case is_exponential:
			Scheduler::rng.seedRand( Scheduler::curSchedule->getSeed() );
			returnValue = Scheduler::rng.getExponentialRand( Scheduler::curSchedule->getPeriod() );
			Scheduler::curSchedule->setSeed( Scheduler::rng.getSeed() );
		break;
	}

	Scheduler::curSchedule++;
	pointerPosition++;

	return returnValue;

}

