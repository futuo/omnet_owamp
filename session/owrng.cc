
#include "owrng.h"
#include <cmath>

/*seed getter*/
unsigned int Owrng::getSeed(){
	return Owrng::seed;
}

/*set rand seed*/
void Owrng::seedRand (unsigned int newseed) {
    Owrng::seed = (unsigned)(newseed & 0x7fffffffU);
}

/*return next random between RAND_MIN and RAND_MAX*/
int Owrng::getRand () {
    Owrng::seed = (Owrng::seed * 1103515245U + 12345U) & 0x7fffffffU;
    return (int)Owrng::seed;
}

/*returns the next random constraint between 0..1*/
double Owrng::getNormalizedRand(){
    int r = Owrng::getRand();
    return  ((double)r)/((double)0x7fffffffU);
}

/*return next random exponentially distributed with mean "mean"*/
double Owrng::getExponentialRand(double mean){
    double r = Owrng::getNormalizedRand();
    return ( -log(r)*mean );
}



