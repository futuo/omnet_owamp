
#include "owsession.h"

OwSession::OwSession( int sessionID, cGate *socketGate, RequestSession *requestSession_m ){
    OwSession::sessionID     = sessionID;
    OwSession::socketGate    = socketGate;
    OwSession::clientAddress = requestSession_m->getClientAddress();
    OwSession::clientPort    = requestSession_m->getClientPort();
    OwSession::serverPort    = requestSession_m->getServerPort();
    OwSession::npackets      = requestSession_m->getNpackets();
    OwSession::probeSize     = requestSession_m->getProbeSize();
    OwSession::timeout       = requestSession_m->getTimeout();
    OwSession::sessionStart  = requestSession_m->getSessionStart();
    
    SlotDescriptor sd;
    for(int i = 0; i<requestSession_m->getNScheduleSlots(); i++ ){
        sd = requestSession_m->getSlotDescriptors(i);

        switch( sd.getDistribution() ){
            case(is_exponential):
                OwSession::scheduler.insertExponentialSchedule( sd.getSeed(), sd.getMean() );
            break;

            case(is_periodic):
                OwSession::scheduler.insertPeriodicSchedule( sd.getMean() );
            break;
        }
    }

    OwSession::results= new OwSessionResults(timeout, npackets);
}

void OwSession::initSession(){
    
    OwProbeInfo *before = NULL, *current = NULL;

    int trainId = 0;

    simtime_t expectedTime = sessionStart;
    /*initialize all the expected packets*/
    for(int i=1; i <= npackets; i++){
        before = current;
        current = new OwProbeInfo( i, expectedTime );

        OwSession::packetHistory.insert(
                std::pair<int, OwProbeInfo*>( i, current ) );

        if( before != NULL ){
            if( current->getExpectedtime() - before->getExpectedtime() == 0 ){

                if( before->getTrainId() == -1 ){
                    current->setTrainId(trainId);
                    before->setTrainId(trainId);
                    trainId++;
                }else{
                    current->setTrainId( before->getTrainId() );
                }
            }
        }

        expectedTime += OwSession::scheduler.getNextValue();
    }

    /*open socket and listen*/
    OwSession::sessionSocket.setOutputGate(OwSession::socketGate);
    OwSession::sessionSocket.bind(OwSession::serverPort);
    OwSession::sessionSocket.setTimeToLive(32);

}

void OwSession::announceProbeReceival( OwProbe *probe, simtime_t receivedtime ){
    
    /*retrieve data from probe packet*/
    int sequenceNumber = probe->getSequenceNumber();
    simtime_t sendtime = probe->getSendTime();
    simtime_t receptionStart = probe->getArrivalTime();
    int ttl = probe->getTtl();
    int probeSize = probe->getProbeSize();


    /*update sequence of received packets*/
    receivalSequence.insert( receivalSequence.end(), sequenceNumber );

    /*update the packet's info*/
    OwProbeInfo *rcv = packetHistory.find(sequenceNumber)->second;

    /*
    std::cout << "Session:" << sessionID << "\tProbe:" << sequenceNumber
            << "\tSent:"<< sendtime << "\tRcvd:" << receivedtime
            << "\tTrain: "<< rcv->getTrainId() << "\tRS: " << probe->getArrivalTime() << std::endl;
    */

    /*if it is not a duplicate*/
    if( rcv->getDuplicateCount() < 0 ){
        rcv->setSendtime(sendtime);
        rcv->setReceivedtime(receivedtime);
        rcv->setReceptionStart(receptionStart);
        rcv->setTtl(ttl);
        rcv->setProbeSize(probeSize);
        rcv->setDuplicateCount(0);
    }else{
        OwProbeInfo *duplicate = new OwProbeInfo();
        duplicate->setSendtime(sendtime);
        duplicate->setReceivedtime(receivedtime);
        duplicate->setReceptionStart(receptionStart);
        duplicate->setTtl(ttl);
        duplicate->setProbeSize(probeSize);
        rcv->insertDuplicate( duplicate );
    }

}

void OwSession::generateResults(){
    OwSession::results->proccessOneWayDelay( packetHistory, timeout );
    OwSession::results->proccessIpPacketDelayVariation( packetHistory, timeout );
    OwSession::results->proccessOneWayPacketLoss( packetHistory );
    OwSession::results->proccessReordered( packetHistory, receivalSequence );
    OwSession::results->proccessSequenceDiscontinuity( packetHistory, receivalSequence );
    OwSession::results->proccessDuplicates( packetHistory );
    OwSession::results->proccessAvailCapacity( packetHistory );
}


/*getters*/
IPvXAddress OwSession::getClientAddress(){
    return OwSession::clientAddress;
}

int OwSession::getNpackets(){
    return OwSession::npackets;
}

int OwSession::getProbeSize(){
    return OwSession::probeSize;
}

int OwSession::getTimeout(){
    return OwSession::timeout;
}

simtime_t OwSession::getSessionStart(){
    return OwSession::sessionStart;
}

int OwSession::getClientPort(){
    return OwSession::clientPort;
}

int OwSession::getServerPort(){
    return OwSession::serverPort;
}

std::map<int,OwProbeInfo*> OwSession::getPacketHistory(){
    return OwSession::packetHistory;
}

std::list<int> OwSession::getReceivalSequence(){
    return OwSession::receivalSequence;
}

SessionState OwSession::getCurState(){
    return curState;
}

int OwSession::getSessionID(){
    return OwSession::sessionID;
}


/*setters*/
void OwSession::setClientAddress( IPvXAddress clientAddress ){
    OwSession::clientAddress = clientAddress;
}

void OwSession::setNpackets( int npackets ){
    OwSession::npackets = npackets;
}

void OwSession::setProbeSize( int probeSize ){
    OwSession::probeSize = probeSize;
}

void OwSession::setTimeout( int timeout ){
    OwSession::timeout = timeout;
}

void OwSession::setSessionStart( simtime_t sessionStart ){
    OwSession::sessionStart = sessionStart;
}

void OwSession::setClientPort(int port){
    OwSession::clientPort = port;
}

void OwSession::setServerPort(int port){
    OwSession::serverPort = port;
}

void OwSession::setCurState(SessionState state){
    OwSession::curState = state;
}

void OwSession::setSessionID(int sessionID){
    OwSession::sessionID = sessionID;
}
