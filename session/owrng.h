#pragma once

class Owrng{

    private:
        unsigned int seed;

    public:
    	unsigned int getSeed();
        void seedRand (unsigned int newseed);
        int getRand ();
        double getNormalizedRand();
        double getExponentialRand(double mean);

};

