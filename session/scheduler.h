//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
// 
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
// 

#pragma once

#include <list>
#include <cstdlib>
#include <cmath>
#include "owrng.h"

enum schedule_t{
    is_exponential, is_uniform, is_periodic
};

class Schedule{

    private:
        schedule_t type;
        unsigned int seed;
        double period;

    protected:
        Schedule(Schedule *);

    public:
        void setType(schedule_t);
        void setSeed(unsigned int);
        void setPeriod(double);

        schedule_t getType();
        unsigned int getSeed();
        double getPeriod();

        Schedule clone();

        Schedule(schedule_t, double);
        Schedule(schedule_t, double, int);
};

class Scheduler{

    private:
        int pointerPosition;
        Owrng rng;
        std::list<Schedule> schedules;
        std::list<Schedule>::iterator curSchedule;

    protected:
        Scheduler(Scheduler *);

    public:
        void insertPeriodicSchedule( double period );
        void insertUniformSchedule( int seed, double average );
        void insertExponentialSchedule( int seed, double average );
        void reset();

        int getNumScheduleSlots();
        std::list<Schedule> getSchedules();
        int getPointerPosition();
        double getNextValue();

        Scheduler clone();

        Scheduler();

};

