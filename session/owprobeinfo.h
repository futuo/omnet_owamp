#pragma once

#include <simtime_t.h>
#include <list>

class OwProbeInfo{

    private:

        int trainId;

        int sequenceNumber;
        int ttl;
        int probeSize;
        int discarded;
        simtime_t sendtime;
        simtime_t receivedtime;
        simtime_t expectedtime;
        simtime_t receptionStart;


        //measurement result fields
        simtime_t   oneWayDelay;
        simtime_t   ipPacketDelayVariation;
        int         isLost;
        int         isReordered;
        int         sequenceDiscontinuity;
        int         duplicateCount;

        std::list<OwProbeInfo *> duplicates;

    public:

        /*getters*/
        int getSequenceNumber();
        int getTtl();
        int getProbeSize();
        simtime_t getSendtime();
        simtime_t getReceivedtime();
        simtime_t getExpectedtime();
        simtime_t getReceptionStart();
        int isDiscarded();
        int getTrainId();

        /*setters*/
        void setSequenceNumber(int sequenceNumber);
        void setTtl(int ttl);
        void setProbeSize(int probeSize);
        void setSendtime(simtime_t sendtime);
        void setReceivedtime(simtime_t receivedtime);
        void setExpectedtime(simtime_t expectedtime);
        void setReceptionStart(simtime_t receptionStart);
        void setDiscarded(int);
        void setTrainId(int);

        /*metric results getters and setters*/
        simtime_t   getOneWayDelay();
        simtime_t   getIpPacketDelayVariation();
        int         getIsLost();
        int         getIsReordered();
        int         getSequenceDiscontinuity();
        int         getDuplicateCount();

        void setOneWayDelay(simtime_t);
        void setIpPacketDelayVariation(simtime_t);
        void setIsLost(int);
        void setIsReordered(int);
        void setSequenceDiscontinuity(int);
        void setDuplicateCount(int);

        void insertDuplicate( OwProbeInfo * );

        /*util methods*/
        int timedOut(int timeout);

        OwProbeInfo();
        OwProbeInfo(int sequenceNumber, simtime_t expectedtime);

};
