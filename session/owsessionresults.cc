
#include "owsessionresults.h"

OwSessionResults::OwSessionResults(int timeout, int npackets){
    OwSessionResults::timeout = timeout;
    OwSessionResults::npackets = npackets;
}

OwSessionResults::~OwSessionResults(){
    if(owdelays)    free(owdelays);
}

void OwSessionResults::proccessOneWayDelay( std::map<int,OwProbeInfo*> packetHistory, int timeout ){
    
    std::map<int,OwProbeInfo*>::iterator it;
    OwProbeInfo *probe;
    int counter = 0;

    for(it = packetHistory.begin(); it != packetHistory.end(); it++){
        probe = it->second;

        if( !probe->timedOut(timeout) && !probe->isDiscarded() ){
            owdelays[counter] = probe->getReceivedtime() - probe->getSendtime();
        }else{
            owdelays[counter] = -1;
        }

        probe->setOneWayDelay(owdelays[counter]);
        counter++;
    }

    simtime_t owdelaysum = 0.0;
    counter = 0;

    for(int i=1; i<npackets; i++){
        if( owdelays[i-1] > 0.0 ){
            owdelaysum += owdelays[i-1];
            counter++;
        }
    }

    std::cout << "------------------------------------------------" << std::endl;
    std::cout << "Average owDelay: " << owdelaysum/counter << std::endl;
    std::cout << "------------------------------------------------" << std::endl;

}

void OwSessionResults::proccessIpPacketDelayVariation( std::map<int,OwProbeInfo*> packetHistory, int timeout ){
    
    std::map<int,OwProbeInfo*>::iterator it;
    OwProbeInfo *probe, *previous = NULL;
    int counter = 0;
    simtime_t ipdv, sum = 0.0;

    for(it = packetHistory.begin(); it != packetHistory.end(); it++){
        
        probe = it->second;

        if( probe->isDiscarded() )
            continue;

        if( probe->getOneWayDelay() >= 0.0 ){
            
            if(previous != NULL){
                ipdv = fabs(probe->getOneWayDelay() - previous->getOneWayDelay());
                probe->setIpPacketDelayVariation(ipdv);
                sum += ipdv;
                counter++;
            }

            previous = probe;
        }

    }

    std::cout << "Average IP packet delay variation: " << sum/counter << std::endl;
    std::cout << "------------------------------------------------" << std::endl;

}

void OwSessionResults::proccessOneWayPacketLoss( std::map<int,OwProbeInfo*> packetHistory ){
    std::map<int,OwProbeInfo*>::iterator it;
    OwProbeInfo *probe;
    int counter = 0;

    for(it = packetHistory.begin(); it != packetHistory.end(); it++){
        
        probe = it->second;

        if( probe->isDiscarded() )
            continue;

        if( probe->getOneWayDelay() < 0.0 ){
            probe->setIsLost(1);
            counter++;
        }else{
            probe->setIsLost(0);
        }

    }

    std::cout << "Number of lost packets: " << counter << std::endl;
    std::cout << "------------------------------------------------" << std::endl;
}

void OwSessionResults::proccessReordered( std::map<int,OwProbeInfo*> packetHistory, std::list<int> receivalSequence ){
    std::list<int>::iterator it;
    OwProbeInfo *probe;
    int counter = 0, seqNo_expected = -1, seqNo_received = -1;

    for(it = receivalSequence.begin(); it != receivalSequence.end(); it++){
        
        seqNo_expected = seqNo_received+1;
        seqNo_received = *it;

        probe = packetHistory.find(seqNo_received)->second;

        if( probe->isDiscarded() )
            continue;

        if( seqNo_received < seqNo_expected ){
            probe->setIsReordered(1);
            counter++;
        }else{
            probe->setIsReordered(0);
        }

    }

    std::cout << "Number of reordered packets: " << counter << std::endl;
    std::cout << "------------------------------------------------" << std::endl;
}

void OwSessionResults::proccessSequenceDiscontinuity( std::map<int,OwProbeInfo*> packetHistory, std::list<int> receivalSequence ){
    std::list<int>::iterator it;
    OwProbeInfo *probe;
    int counter = 0, seqNo_expected = -1, seqNo_received = 0;

    std::cout << "Sequence discontinuities: (id, size)";

    for(it = receivalSequence.begin(); it != receivalSequence.end(); it++){
        
        seqNo_expected = seqNo_received+1;
        seqNo_received = *it;

        probe = packetHistory.find(seqNo_received)->second;

        if( probe->isDiscarded() )
            continue;

        if( seqNo_received > seqNo_expected ){
            probe->setSequenceDiscontinuity( seqNo_received - seqNo_expected );
            std::cout << "(" << seqNo_expected << "," << seqNo_received - seqNo_expected << ")";
            counter++;
        }else{
            probe->setIsReordered(0);
        }

    }

    std::cout << std::endl << "------------------------------------------------" << std::endl;
}

void OwSessionResults::proccessDuplicates( std::map<int,OwProbeInfo*> packetHistory ){
    std::map<int,OwProbeInfo*>::iterator it;
    OwProbeInfo *probe;
    int counter = 0;

    for(it = packetHistory.begin(); it != packetHistory.end(); it++){
        
        probe = it->second;

        if( probe->isDiscarded() )
            continue;

        if( probe->getDuplicateCount() > 0 ){
            counter += probe->getDuplicateCount();
        }

    }

    std::cout << "Number of duplicated packets: " << counter << std::endl;
    std::cout << "------------------------------------------------" << std::endl;
}

void OwSessionResults::proccessAvailCapacity( std::map<int, OwProbeInfo*> packetHistory){

    /* TODO
     * 1. Make this work if the channel is configured to set the received time
     *    as the moment the packet started being received
     */

    std::map<int,OwProbeInfo*>::iterator it;
    OwProbeInfo *probe, *bef, *cur;

    double trainSize = 0;
    simtime_t i_begin, i_end;
    double availCapacity = 0;

    for(it = packetHistory.begin(); it != packetHistory.end();){

        probe = it->second;

        if( probe->isDiscarded() == 1 ){
            it++;
            continue;
        }

        if( probe->getTrainId() != -1 ){

            i_begin = probe->getReceivedtime();
            trainSize = 0;

            bef = probe;
            cur = (++it)->second;

            while( (cur->isDiscarded() || cur->getIsLost()) && it != packetHistory.end() )
                cur = (++it)->second;

            while( cur->getTrainId() == bef->getTrainId() && it != packetHistory.end() ){
                trainSize += cur->getProbeSize();
                bef = cur;
                cur = (++it)->second;
                while( (cur->isDiscarded() || cur->getIsLost()) && it != packetHistory.end() )
                    cur = (++it)->second;
            }

            i_end = bef->getReceivedtime();
            availCapacity = trainSize/(i_end - i_begin);

            double A,B = 16 * 1000 * 1000,Pb = B,N,S;
            simtime_t IS, T;

            N = 20;
            S = 1440*8;

            T = (i_end - i_begin - (S/B)*(N-1))/(N-1);

            A = B - ( (Pb*(B*T.dbl() - N*S))/((N-1)*S) );

            A = ( B * ( (i_end - i_begin)/(N-1) ) - (B * T) ) / ( (i_end - i_begin)/(N-1) );


//            availCapacity = availCapacity - ( probe->getSendtime() )

/*
            double B = 16 * 1000 * 1000,
                   N = 20,
                   S = 1440 * 8;

            simtime_t D = (i_end - i_begin - (S/B)*(N-1))/(N-1);

            std::cout << D << std::endl;

            std::cout << (D*B + S) << std::endl;
            std::cout << ( (D.dbl()+1)*B ) << std::endl;
            std::cout << ( (D.dbl()+1)*B )/(D*B + S) << std::endl;

            double Nd = ( (D.dbl()+1)*B )/(D*B + S);

            std::cout << Nd << std::endl;

            double A = B - B*(D.dbl()*(Nd-1));

            std::cout << "utilization :" << B*(D.dbl()*(Nd-1)) /1000/1000 << " Mbps" << std::endl;
*/
            std::cout << "avail capacity between " << i_begin << " and "
                    << i_end << ": " << A/1000/1000 << "Mbps"
                            << std::endl;

/*            double CT = ( B - A ) * (N*S/Pb);

            std::cout << "cross traffic between" << i_begin << " and "
                    << i_end << ": " << CT/1000/1000 << "Mbps"
                            << std::endl;
*/


        }else{
            it++;
        }

    }

    std::cout << "------------------------------------------------" << std::endl;

}
