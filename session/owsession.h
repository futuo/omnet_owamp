#pragma once

#include <list>
#include <cstdio>

#include <simtime_t.h>
#include "IPvXAddressResolver.h"
#include "UDPSocket.h"

#include "owsessionresults.h"
#include "owrng.h"
#include "owprobeinfo.h"
#include "scheduler.h"
#include "owprobe_m.h"
#include "requestSession_m.h"
#include "UDPControlInfo_m.h"


enum SessionState{

    SESSION_REQUEST_RECEIVED,
    SESSION_START_RECEIVED,
    SESSION_STARTED,
    SESSION_STOPPED,

};

class OwSession{

    private:

        /*session state info*/
        SessionState curState;

        /*session info*/
        int sessionID;
        int npackets;   
        int probeSize;
        int timeout;
        simtime_t sessionStart;

        Scheduler scheduler;

        /*connection info*/
        int clientPort;
        int serverPort;
        IPvXAddress clientAddress;
        UDPSocket sessionSocket;

        /*session data collection*/
        std::map<int,OwProbeInfo*> packetHistory;
        //TODO change receivalSequence to a list of pointers to OwProbeInfo
        std::list<int> receivalSequence;

        /*session results calculator and store struct*/
        OwSessionResults *results;

        /*session needed fields*/
        int lastPacketSeqNo;
        Owrng sessionRng;

        /*associated server*/
        cGate* socketGate;

    public:

        void initSession();
        void announceProbeReceival( OwProbe *, simtime_t );
        void generateResults();

        IPvXAddress getClientAddress();
        int getNpackets();
        int getProbeSize();
        int getTimeout();
        simtime_t getSessionStart();
        int getSeed();
        double getExpMean();
        int getClientPort();
        int getServerPort();
        std::map<int,OwProbeInfo*> getPacketHistory();
        std::list<int> getReceivalSequence();
        SessionState getCurState();
        int getSessionID();

        void setClientAddress( IPvXAddress clientAddress );
        void setNpackets( int npackets );
        void setProbeSize( int probeSize );
        void setTimeout( int timeout );
        void setSessionStart( simtime_t sessionStart );
        void setSeed(int seed);
        void setExpMean(double expMean);
        void setClientPort(int port);
        void setServerPort(int port);
        void setCurState(SessionState state);
        void setSessionID(int);

        OwSession( int, cGate*, RequestSession* );

};

