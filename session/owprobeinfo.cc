#include "owprobeinfo.h"
#include <omnetpp.h>
#include <cstdio>

OwProbeInfo::OwProbeInfo(){
    OwProbeInfo::sequenceNumber = 0;
    OwProbeInfo::ttl = 0;
    OwProbeInfo::probeSize = 0;
    OwProbeInfo::sendtime = 0;
    OwProbeInfo::receivedtime = 0;
    OwProbeInfo::expectedtime = 0;
    OwProbeInfo::duplicateCount = -1;
    OwProbeInfo::discarded = 0;
    OwProbeInfo::trainId = -1;
    OwProbeInfo::receptionStart = 0;
}

OwProbeInfo::OwProbeInfo(int sequenceNumber, simtime_t expectedtime){
    OwProbeInfo::sequenceNumber = sequenceNumber;
    OwProbeInfo::ttl = 0;
    OwProbeInfo::probeSize = 0;
    OwProbeInfo::sendtime = 0;
    OwProbeInfo::receivedtime = 0;
    OwProbeInfo::expectedtime = expectedtime;
    OwProbeInfo::duplicateCount = -1;
    OwProbeInfo::discarded = 0;
    OwProbeInfo::trainId = -1;
    OwProbeInfo::receptionStart = 0;
}

/*getters*/
int OwProbeInfo::getSequenceNumber(){
    return OwProbeInfo::sequenceNumber;
}

int OwProbeInfo::getTtl(){
    return OwProbeInfo::ttl;
}

int OwProbeInfo::getProbeSize(){
    return OwProbeInfo::probeSize;
}

simtime_t OwProbeInfo::getSendtime(){
    return OwProbeInfo::sendtime;
}

simtime_t OwProbeInfo::getReceivedtime(){
    return OwProbeInfo::receivedtime;
}

simtime_t OwProbeInfo::getExpectedtime(){
    return OwProbeInfo::expectedtime;
}

simtime_t OwProbeInfo::getReceptionStart(){
    return OwProbeInfo::receptionStart;
}

int OwProbeInfo::isDiscarded(){
    return OwProbeInfo::discarded;
}

int OwProbeInfo::getTrainId(){
    return OwProbeInfo::trainId;
}

/*setters*/
void OwProbeInfo::setSequenceNumber(int sequenceNumber){
    OwProbeInfo::sequenceNumber = sequenceNumber;
}

void OwProbeInfo::setTtl(int ttl){
    OwProbeInfo::ttl = ttl;
}

void OwProbeInfo::setProbeSize(int probeSize){
    OwProbeInfo::probeSize = probeSize;
}

void OwProbeInfo::setSendtime(simtime_t sendtime){
    OwProbeInfo::sendtime = sendtime;
}

void OwProbeInfo::setReceivedtime(simtime_t receivedtime){
    OwProbeInfo::receivedtime = receivedtime;
}

void OwProbeInfo::setExpectedtime(simtime_t expectedtime){
    OwProbeInfo::expectedtime = expectedtime;
}

void OwProbeInfo::setReceptionStart(simtime_t receptionStart){
    OwProbeInfo::receptionStart = receptionStart;
}

void OwProbeInfo::setDiscarded(int discarded){
    OwProbeInfo::discarded = discarded;
}

void OwProbeInfo::setTrainId(int trainId){
    OwProbeInfo::trainId = trainId;
}

/*metric results getters and setters*/
simtime_t OwProbeInfo::getOneWayDelay(){
    return OwProbeInfo::oneWayDelay;
}

simtime_t OwProbeInfo::getIpPacketDelayVariation(){
    return OwProbeInfo::ipPacketDelayVariation;
}

int OwProbeInfo::getIsLost(){
    return OwProbeInfo::isLost;
}

int OwProbeInfo::getIsReordered(){
    return OwProbeInfo::isReordered;
}

int OwProbeInfo::getSequenceDiscontinuity(){
    return OwProbeInfo::sequenceDiscontinuity;
}

int OwProbeInfo::getDuplicateCount(){
    return OwProbeInfo::duplicateCount;
}


void OwProbeInfo::setOneWayDelay(simtime_t oneWayDelay){
    OwProbeInfo::oneWayDelay = oneWayDelay;
}

void OwProbeInfo::setIpPacketDelayVariation(simtime_t ipPacketDelayVariation){
    OwProbeInfo::ipPacketDelayVariation = ipPacketDelayVariation;
}

void OwProbeInfo::setIsLost(int isLost){
    OwProbeInfo::isLost = isLost;
}

void OwProbeInfo::setIsReordered(int isReordered){
    OwProbeInfo::isReordered = isReordered;
}

void OwProbeInfo::setSequenceDiscontinuity(int sequenceDiscontinuity){
    OwProbeInfo::sequenceDiscontinuity = sequenceDiscontinuity;
}

void OwProbeInfo::setDuplicateCount(int duplicateCount){
    OwProbeInfo::duplicateCount = duplicateCount;
}



void OwProbeInfo::insertDuplicate( OwProbeInfo * duplicate){
    OwProbeInfo::duplicates.insert( duplicates.end(), duplicate );
    duplicateCount++;
}


/*util methods*/

int OwProbeInfo::timedOut(int timeout){
    
    /*if the packet hasn't yet arrived*/
    if( OwProbeInfo::receivedtime == 0 ){
        /*if the packet has timed out*/
        if( OwProbeInfo::expectedtime + timeout <= simTime() ){
            return 1;
        }

    /*if the packet has already arrived*/
    }else{
        /*but it arrived after timing out*/
        if( OwProbeInfo::expectedtime + timeout <= OwProbeInfo::receivedtime )
            return 1;
    }
    /*in all the other cases*/
    return 0;
}

