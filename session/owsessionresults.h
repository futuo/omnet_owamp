//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
// 
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
// 

#pragma once

#include <map>
#include <list>
#include <cstdio>

#include "owprobeinfo.h"

class OwSessionResults{

  private:
    simtime_t owdelays[1000000];
    int timeout;
    int npackets;

  public:
    void proccessOneWayDelay( std::map<int,OwProbeInfo*>, int );
    void proccessIpPacketDelayVariation( std::map<int,OwProbeInfo*>, int );
    void proccessOneWayPacketLoss( std::map<int,OwProbeInfo*> );
    void proccessReordered( std::map<int,OwProbeInfo*>, std::list<int> );
    void proccessSequenceDiscontinuity( std::map<int,OwProbeInfo*>, std::list<int> );
    void proccessDuplicates( std::map<int,OwProbeInfo*> );
    void proccessAvailCapacity( std::map<int, OwProbeInfo*> );

    OwSessionResults(int, int);
    ~OwSessionResults();
};

